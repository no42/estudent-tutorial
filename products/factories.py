import random
from django.contrib.auth.models import User
from django.utils.text import slugify
from factory import Faker, Maybe, RelatedFactoryList, LazyAttribute, PostGenerationMethodCall
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice
from .models import Category, Brand, Product, Order, OrderProduct


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    first_name = Faker('first_name', locale='hr_HR')
    last_name = Faker('last_name', locale='hr_HR')
    email = Faker('email', locale='hr_HR')
    username = LazyAttribute(lambda o: slugify(f'{o.first_name.lower()[0]}{o.last_name.lower()}'))
    password = PostGenerationMethodCall('set_password', 'pass')


class CategoryFactory(DjangoModelFactory):
    class Meta:
        model = Category

    title = Faker('word')


class BrandFactory(DjangoModelFactory):
    class Meta:
        model = Brand

    title = Faker('name')
    premium = Faker('boolean', chance_of_getting_true=33)


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = Product

    category = FuzzyChoice(Category.objects.all())
    brand = FuzzyChoice(Brand.objects.all())
    title = Faker('name')
    price = Faker('pydecimal', right_digits=2, positive=True, min_value=100, max_value=2000)


class OrderProductFactory(DjangoModelFactory):
    class Meta:
        model = OrderProduct

    product = FuzzyChoice(Product.objects.all())
    quantity = Faker('pyint', min_value=1, max_value=10)


class OrderFactory(DjangoModelFactory):
    class Meta:
        model = Order

    class Params:
        has_user = Faker('boolean', chance_of_getting_true=50)

    user = Maybe('has_user', yes_declaration=FuzzyChoice(User.objects.filter(is_staff=False)))
    products = RelatedFactoryList(OrderProductFactory, 'order', size=lambda: random.randint(1, 10))
