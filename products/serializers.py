from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer, CharField, DecimalField
from .models import Product, Order, Brand, OrderProduct


class BrandSerializer(ModelSerializer):
    class Meta:
        model = Brand
        fields = ['title', 'premium']


class ProductSerializer(ModelSerializer):
    category = CharField(source='category.title')
    brand = BrandSerializer()

    class Meta:
        model = Product
        fields = ['id', 'title', 'brand', 'category', 'price']


class OrderProductSerializer(ModelSerializer):
    class Meta:
        model = OrderProduct
        fields = ['product', 'quantity']


class OrderSerializer(ModelSerializer):
    total = DecimalField(max_digits=10, decimal_places=2, read_only=True)
    products = OrderProductSerializer(many=True)

    class Meta:
        model = Order
        fields = ['id', 'user', 'total', 'products', 'created']
        read_only_fields = ['user', 'created']

    def create(self, validated_data):
        request = self.context['request']
        products = validated_data.pop('products')
        order = Order.objects.create(user=request.user if request.user.is_authenticated else None)
        for product in products:
            OrderProduct.objects.create(order=order, **product)
        return order

    def update(self, instance, validated_data):
        products = validated_data.pop('products')
        instance.products.all().delete()
        for product in products:
            OrderProduct.objects.create(order=instance, **product)
        return instance


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'is_active']
        read_only_fields = ['username']
