from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from products import factories, models


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        if not settings.DEBUG:
            return

        print('initialize started')
        User.objects.exclude(is_superuser=True).delete()
        models.Order.objects.all().delete()
        models.Product.objects.all().delete()
        models.Brand.objects.all().delete()
        models.Category.objects.all().delete()
        print('removed old data')
        factories.UserFactory.create_batch(size=2, is_staff=True)
        print('staff created')
        factories.UserFactory.create_batch(size=10)
        print('customers created')
        factories.CategoryFactory.create_batch(size=3)
        print('product categories created')
        factories.BrandFactory.create_batch(size=10)
        print('brands created')
        factories.ProductFactory.create_batch(size=500)
        print('products created')
        factories.OrderFactory.create_batch(size=50)
        print('orders created')
        print('-'*80)
        print('DONE')
