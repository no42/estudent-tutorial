from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from .models import Order, Product, Brand
from .permissions import OrderPermissions, UserPermissions
from .serializers import OrderSerializer, ProductSerializer, UserSerializer, BrandSerializer


class ProductListView(ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        return Product.objects.all()


class BrandFilter(FilterSet):
    class Meta:
        model = Brand
        fields = ['premium']


class BrandListView(ListAPIView):
    serializer_class = BrandSerializer
    filterset_class = BrandFilter
    filter_backends = [DjangoFilterBackend]

    def get_queryset(self):
        return Brand.objects.all()


class OrderViewSet(ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = [OrderPermissions]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['created']

    def get_queryset(self):
        qs = Order.objects.all()
        if self.action == 'list':
            user = self.request.user
            if user.is_authenticated and not user.is_staff:
                qs = qs.filter(user=user)
        return qs


class UserViewSet(ListModelMixin, RetrieveModelMixin, UpdateModelMixin, GenericViewSet):
    serializer_class = UserSerializer
    permission_classes = [UserPermissions]
    filter_backends = [filters.SearchFilter]
    search_fields = ['first_name', 'last_name', 'email']

    def get_queryset(self):
        return User.objects.all()
