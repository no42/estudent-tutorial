from django.contrib import admin
from .models import Category, Brand, Product, Order, OrderProduct


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    ...


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['title', 'brand', 'category', 'price']
    list_filter = ['brand', 'category']


class OrderProductInline(admin.TabularInline):
    model = OrderProduct


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderProductInline]
    list_display = ['get_name', 'user', 'total']

    def get_name(self, obj):
        return f'Order {obj.id}'
    get_name.short_description = 'name'
    get_name.admin_order_field = 'id'


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ['title', 'premium']
